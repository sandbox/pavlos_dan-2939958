<?php

/**
 * We need to do this as Drupal does not let us uninstall the module without purging the field contents first.
 **/

use Drupal\multiversion\Entity\Workspace;
use Psr\Log\LogLevel;

/**
 * Implements of hook_drush_command().
 */
function workspace_multiple_remotes_drush_command() {
  $items = [];

  $items['workspace-multiple-remotes-uninstall'] = [
    'bootstrap' => DRUSH_BOOTSTRAP_NONE,
    'description' => 'Uninstall Workspace Multiple Remotes.',
    'aliases' => ['wmrun'],
  ];

  return $items;
}

/**
 * Implements drush_hook_COMMAND().
 */
function drush_workspace_multiple_remotes_uninstall() {
  //Stolen from workspace
  $extension = 'workspace_multiple_remotes';
  $uninstall = TRUE;
  $extension_info = drush_get_extensions();
  $required = drush_drupal_required_modules($extension_info);
  if (in_array($extension, $required)) {
    $info = $extension_info[$extension]->info;
    $explanation = !empty($info['explanation']) ? ' ' . dt('Reason: !explanation.', ['!explanation' => strip_tags($info['explanation'])])  : '';
    drush_log(dt('!extension is a required extension and can\'t be uninstalled.', ['!extension' => $extension]) . $explanation, LogLevel::INFO);
    $uninstall = FALSE;
  }
  elseif (!$extension_info[$extension]->status) {
    drush_log(dt('!extension is already uninstalled.', ['!extension' => $extension]), LogLevel::INFO);
    $uninstall = FALSE;
  }
  elseif (drush_extension_get_type($extension_info[$extension]) == 'module') {
    $dependents = [];
    foreach (drush_module_dependents([$extension], $extension_info) as $dependent) {
      if (!in_array($dependent, $required) && ($extension_info[$dependent]->status)) {
        $dependents[] = $dependent;
      }
    }
    if (count($dependents)) {
      drush_log(dt('To uninstall !extension, the following extensions must be uninstalled first: !required', ['!extension' => $extension, '!required' => implode(', ', $dependents)]), LogLevel::ERROR);
      $uninstall = FALSE;
    }
  }

  if ($uninstall) {
    drush_print(dt('Workspace Multiple Remotes will be uninstalled.'));
    if(!drush_confirm(dt('Do you really want to continue?'))) {
      return drush_user_abort();
    }

    try {
      //https://drupal.stackexchange.com/questions/230956/cant-uninstall-module-because-of-checkbox-field/239355
      $query = \Drupal::entityQuery('workspace');
      $result = $query->execute();
  
      $workspaces = \Drupal::entityTypeManager()->getStorage('workspace')->loadMultiple($result);
  
      foreach ($workspaces as $workspace) {
        if ($workspace instanceof Workspace) {
          $workspace->set('additional_targets', NULL);
          $workspace->save();
        }
      }

      drush_module_uninstall([$extension]);
      // Inform the user of final status.
      drush_log(dt('!extension was successfully uninstalled.', ['!extension' => $extension]), LogLevel::INFO);
    }
    catch (Exception $e) {
      drush_log($e->getMessage(), LogLevel::ERROR);
    }
  }

}
