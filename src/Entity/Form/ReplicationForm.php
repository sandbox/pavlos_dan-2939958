<?php

namespace Drupal\workspace_multiple_remotes\Entity\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\replication\Entity\ReplicationLogInterface;
use Drupal\workspace\Entity\Replication;
use Drupal\workspace\WorkspacePointerInterface;
use  Drupal\deploy\Entity\Form\ReplicationForm as DeployReplicationForm;

/**
 * Form controller for Replication edit forms.
 *
 * @ingroup deploy
 */
class ReplicationForm extends DeployReplicationForm { //ContentEntityForm {
  
  /** @var  [] */
  protected $targets = null;

  public function addTitle() {
    $this->setEntity(Replication::create());
    if (!$this->getDefaultSource() || !$this->getDefaultTarget()) {
      return $this->t('Error');
    }
    //Why is this override not working?
    return $this->t('Deploy @source to multiple targets', [
      '@source' => $this->getDefaultSource()->label()
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $input = $form_state->getUserInput();
    
    //Turn off Ajax(?)
    if (isset($input['_drupal_ajax'])) {
      $input['_drupal_ajax'] = FALSE;
      $form_state->setUserInput($input);
    }
    
    //Use the DeployReplicationForm builder
    $form = parent::buildForm($form, $form_state);
    
    /**
     * TODO: Should probably check that one of our targets is the default target!
     * Maybe enforce it even with form states.
     **/
    $additional_targets = $this->getAdditionalTargets();
    if (!empty($additional_targets)) {
      drupal_set_message($this->t("Multiple target deployment."));
    }

    $form['actions']['submit']['#value'] = $this->t('Deploy');
    //Do not ajax submit.
    unset($form['actions']['submit']['#ajax']);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    // Pass the abort flag to the ReplicationManager using runtime-only state,
    // i.e. a static.
    // @see \Drupal\workspace\ReplicatorManager
	  // @see \Drupal\workspace\Entity\Form\WorkspaceForm
    $is_aborted_on_conflict = !$form_state->hasValue('is_aborted_on_conflict') || $form_state->getValue('is_aborted_on_conflict') === 'true';
    drupal_static('workspace_is_aborted_on_conflict', $is_aborted_on_conflict);
    
    //Do NOT use the parent deploy save function. Skip to ContentEntityForm::Save instead otherwise we risk deploying twice.
    //We are effectively hijacking the save process.
    ContentEntityForm::save($form, $form_state);

    $targets = $this->getAdditionalTargets();

    $input = $form_state->getUserInput();

    try {
      //Start setting up our batch process
      $operations = [];
      //One operation for each of our targets.
      //TODO: Can we have a batch within a batch? Can the replication start using batches for replicating the entities?
      //Follow up with: https://www.drupal.org/project/workspace/issues/2683309
      //Without it we get the impression that the operations hang until the whole deploy is complete.
      //We also need better error handling
      foreach($targets as $key => $target_pointer) {

        //Get the remote workspace pointer
        $target = \Drupal::service('entity_type.manager')
           ->getStorage('workspace_pointer')
           ->load($target_pointer['target_id']);
        
        $source = $this->entity->get('source')->entity;

        $operations[] = ['workspace_multiple_remotes_batch_replicate', [$source, $target, $this->entity]];
      }

      $batch = [
        'operations' => $operations,
        'finished' => 'workspace_multiple_remotes_batch_replicate_finish',
        'init_message' => $this->t("Deploying..."),
      ];

      batch_set($batch);
    }
    catch(\Exception $e) {
      watchdog_exception('Deploy', $e);
      drupal_set_message($e->getMessage(), 'error');
    }
  }
  
  /**
   * Get our multiple targets.
   **/
  protected function getAdditionalTargets() {
    if (!empty($this->targets)) {
      return $this->targets;
    }
    
    $source_workspace = $this->getDefaultSource()->getWorkspace();
    $additional_targets = $source_workspace->get('additional_targets')->getValue();
    return $additional_targets;
  }

}
